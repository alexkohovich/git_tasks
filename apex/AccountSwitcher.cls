/**
 * Created by Alex on 1/17/2019.
 */

public interface AccountSwitcher {

	public void methodB() {
//		more of code in methodB;
	}
	
	public void methodA() {
//		code methodA;
	}

	void getAndSort();
	AccountSwitcherImpl.DataResult switchAccount();

}